<?php

namespace Drupal\translate_side_by_side\Form;

use Drupal\Core\Entity\EntityFieldManager;
use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Extension\ModuleHandler;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageManager;
use Drupal\Core\Menu\MenuLinkTree;
use Drupal\Core\Menu\MenuTreeParameters;
use Drupal\Core\Render\Renderer;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form to configure module settings.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The variable containing the language manager.
   *
   * @var \Drupal\Core\Language\LanguageManager
   */
  protected $languageManager;

  /**
   * The variable containing the entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /**
   * The variable containing the entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManager
   */
  protected $entityFieldManager;

  /**
   * The variable containing the menu tree link.
   *
   * @var \Drupal\Core\Menu\MenuLinkTree
   */
  protected $menuLinkTree;

  /**
   * The variable containing the renderer.
   *
   * @var \Drupal\Core\Render\Renderer
   */
  protected $renderer;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandler
   */
  protected $moduleHandler;

  /**
   * Source language.
   *
   * @var string
   */
  protected $sourceLanguage;

  /**
   * Target language.
   *
   * @var string
   */
  protected $targetLanguage;

  /**
   * Dependency injection through the constructor.
   *
   * @param \Drupal\Core\Language\LanguageManager $languageManager
   *   The language service.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type service.
   * @param \Drupal\Core\Entity\EntityFieldManager $entityFieldManager
   *   The entity field service.
   * @param \Drupal\Core\Menu\MenuLinkTree $menuLinkTree
   *   The menu link tree  service.
   * @param \Drupal\Core\Render\Renderer $renderer
   *   The renderer service.
   * @param \Drupal\Core\Extension\ModuleHandler $moduleHandler
   *   The module handler.
   */
  public function __construct(
    LanguageManager $languageManager,
    EntityTypeManager $entityTypeManager,
    EntityFieldManager $entityFieldManager,
    MenuLinkTree $menuLinkTree,
    Renderer $renderer,
    ModuleHandler $moduleHandler,
  ) {
    $this->languageManager = $languageManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->entityFieldManager = $entityFieldManager;
    $this->menuLinkTree = $menuLinkTree;
    $this->renderer = $renderer;
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * Dependency injection create.
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('language_manager'),
    $container->get('entity_type.manager'),
    $container->get('entity_field.manager'),
    $container->get('menu.link_tree'),
    $container->get('renderer'),
    $container->get('module_handler'));
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'translate_side_by_side.settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['config.translate_side_by_side'];
  }

  /**
   * Build form.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = [];

    $form['selection'] = [
      '#type' => 'fieldset',
    ];
    $form['selection']['source'] = [
      '#title' => $this->t('Source language'),
      '#type' => 'language_select',
      '#languages' => Language::STATE_CONFIGURABLE,
    ];
    $form['selection']['target'] = [
      '#title' => $this->t('Target language'),
      '#type' => 'language_select',
      '#languages' => Language::STATE_CONFIGURABLE,
    ];
    $form['selection']['skipempty'] = [
      '#title' => $this->t('Skip field, if empty in source'),
      '#type' => 'checkbox',
    ];
    $form['selection']['filluntranslated'] = [
      '#title' => $this->t('Fill untranslated with source'),
      '#type' => 'checkbox',
    ];
    if ($this->moduleHandler->moduleExists('node')) {
      $contentTypes = $this->entityTypeManager->getStorage('node_type')->loadMultiple();
      $contentTypesList = [];
      foreach ($contentTypes as $contentType) {
        $contentTypesList[$contentType->id()] = $contentType->label();
      }
      $form['selection']['node_types'] = [
        '#title' => $this->t('Content types'),
        '#type' => 'select',
        '#multiple' => TRUE,
        '#size' => 10,
        '#options' => $contentTypesList,
      ];
    }
    $form['selection']['load'] = [
      '#type' => 'button',
      '#value' => $this->t('Load'),
    ];

    $this->sourceLanguage = ($form_state->getValue('source') != '') ? ($form_state
      ->getValue('source')) : ($this->languageManager->getDefaultLanguage()
      ->getId());
    $this->targetLanguage = ($form_state->getValue('target') != '') ? ($form_state
      ->getValue('target')) : ($this->languageManager->getDefaultLanguage()
      ->getId());

    if ($this->moduleHandler->moduleExists('menu_link_content')) {
      $form['menus'] = [
        '#value' => $this->t('Menus'),
        '#type' => 'html_tag',
        '#tag' => 'h3',
      ];
      $form = $this->buildFormMenu($form, $form_state);
    }

    if ($this->moduleHandler->moduleExists('node')) {
      $form['nodes'] = [
        '#value' => $this->t('Nodes'),
        '#type' => 'html_tag',
        '#tag' => 'h3',
      ];
      $form = $this->buildFormEntity($form, $form_state, 'node', 'title', 'nid');
    }

    if ($this->moduleHandler->moduleExists('block_content')) {
      $form['blocks'] = [
        '#value' => $this->t('Blocks'),
        '#type' => 'html_tag',
        '#tag' => 'h3',
      ];
      $form = $this->buildFormEntity($form, $form_state, 'block_content', 'info', 'id');
    }

    if ($this->moduleHandler->moduleExists('taxonomy')) {
      $form['taxonomy'] = [
        '#value' => $this->t('Taxonomy'),
        '#type' => 'html_tag',
        '#tag' => 'h3',
      ];
      $form = $this->buildFormTaxonomy($form, $form_state);
    }

    $form = parent::buildForm($form, $form_state);

    // Remove submit button.
    unset($form['actions']['submit']);
    return $form;
  }

  /**
   * Build entity form.
   */
  private function buildFormEntity(
    array $form,
    FormStateInterface $form_state,
    $entityType,
    $sortBy,
    $idKey,
  ) {
    $query = $this->entityTypeManager->getStorage($entityType)->getQuery()
      ->accessCheck(FALSE)
      ->condition('langcode', 'zzz', '<>')
      ->condition('langcode', 'zxx', '<>');
    if ($entityType == 'node' && !empty($form_state->getValue('node_types'))) {
      $filter = $query->orConditionGroup();
      foreach ($form_state->getValue('node_types') as $key => $value) {
        $filter->condition('type', $key);
      }
      $query = $query->condition($filter);
    }
    $nids = $query->sort($sortBy, 'ASC', 'en')
      ->execute();
    $entities = $this->entityTypeManager->getStorage($entityType)->loadMultiple($nids);
    /** @var \Drupal\Core\Entity\ContentEntityBase $oneEntity */
    foreach ($entities as $oneEntity) {
      if ($oneEntity->hasTranslation($this->sourceLanguage)) {
        $original = $oneEntity->getTranslation($this->sourceLanguage);
      }
      else {
        $original = $oneEntity;
      }
      if ($oneEntity->hasTranslation($this->targetLanguage)) {
        $translation = $oneEntity->getTranslation($this->targetLanguage);
      }
      else {
        $translation = NULL;
      }

      $fieldlist = [];
      /** @var \Drupal\Core\Entity\EntityDisplayBase $entity_form_display */
      $entity_form_display = $this->entityTypeManager
        ->getStorage('entity_form_display')
        ->load($entityType . '.' . $oneEntity->bundle() . '.default');
      $fields_in_display = $entity_form_display->getComponents();
      uasort($fields_in_display, [
        'Drupal\Component\Utility\SortArray',
        'sortByWeightElement',
      ]);
      foreach ($fields_in_display as $field_name => $field_definition) {
        if (isset($field_definition['type'])) {
          $fieldlist = array_merge($fieldlist, $this->retrieveField($field_definition['type'], $original, $translation, $field_name, $oneEntity, $form_state));
        }
      }
      $form['tablet' . $entityType][] = $this->buildFormTable($form, $form_state,
      $oneEntity->get($sortBy)->value . ' ' . str_replace('/' .
      $this->languageManager->getDefaultLanguage()->getId(), '',
      $oneEntity->toUrl()->toString()), $entityType . ' ID: ' . $oneEntity->get($idKey)->value,
      $fieldlist, $entityType . $oneEntity->get($idKey)->value);

    }
    return $form;
  }

  /**
   * Build field.
   */
  private function retrieveField($field_type, $original, $translation, $field_name, $oneEntity, $form_state) {
    $fieldlist = [];
    if (!($field_name == 'title' || $field_name == 'body' || $field_name == 'info' ||
      substr($field_name, 0, 6) === 'field_')) {
      return [];
    }
    if ($original->get($field_name)->count() == 0) {
      // Field_group exception.
      $entlist = [$original->get($field_name)];
    }
    else {
      $entlist = $original->get($field_name);
    }

    foreach ($entlist as $onekey => $onevalue) {
      $txtattr = [];

      if (substr($field_type, 0, 5) == 'image') {
        $txtattr[] = 'alt';
        $txtattr[] = 'title';
      }
      elseif (substr($field_type, 0, 4) == 'link') {
        $txtattr[] = 'title';
      }
      elseif (substr($field_type, 0, 4) == 'file') {
        $txtattr[] = 'description';
      }
      elseif (substr($field_type, 0, 7) === 'string_'
      || substr($field_type, 0, 5) === 'text_') {
        $txtattr[] = 'value';
      }
      elseif ($field_type === 'paragraphs_classic_asymmetric'  || $field_type === 'entity_reference_revisions'
      || $field_type === 'entity_reference_revisions_entity_view' || $field_type === 'entity_reference_entity_view' || $field_type === 'paragraph_summary'
      || $field_type === 'field_collection_list' || $field_type === 'field_collection_items') {
        $fieldlist = array_merge($fieldlist, $this->retrieveParagraphs($field_type, $original, $translation, $field_name, $oneEntity, $form_state));
        return $fieldlist;
      }

      foreach ($txtattr as $onetxtattr) {
        if ($form_state->getValue('skipempty') !== 0
        && empty($original->get($field_name)->getValue()[$onekey][$onetxtattr])) {
          continue;
        }
        if ($translation !== NULL && isset($translation->get($field_name)
          ->getValue()[$onekey]) && isset($translation->get($field_name)
          ->getValue()[$onekey][$onetxtattr])) {
          $field_val = $translation->get($field_name)
            ->getValue()[$onekey][$onetxtattr];
        }
        elseif ($form_state->getValue('filluntranslated') === 0) {
          $field_val = '';
        }
        elseif (isset($original->get($field_name)->getValue()[$onekey])) {
          $field_val = $original->get($field_name)
            ->getValue()[$onekey][$onetxtattr];
        }
        else {
          $field_val = '';
        }
        $fieldlist[] = [
          'f' => $field_name,
          'n' => (($onetxtattr !== 'value') ? (' (' . $onetxtattr . ')') : ('')),
          's' => (isset($original->get($field_name)->getValue()[$onekey])) ? ($original->get($field_name)->getValue()[$onekey][$onetxtattr]) : (''),
          't' => $field_val,
        ];
      }
    }
    return $fieldlist;
  }

  /**
   * Build paragraphs field.
   */
  private function retrieveParagraphs($field_type, $original, $translation, $field_name, $oneEntity, $form_state) {
    $par_fieldlist = [];
    foreach ($original->get($field_name)->referencedEntities() as $oneparvalue) {
      if ($oneparvalue->hasTranslation($this->sourceLanguage)) {
        $par_original = $oneparvalue->getTranslation($this->sourceLanguage);
      }
      else {
        $par_original = NULL;
      }
      if ($oneparvalue->hasTranslation($this->targetLanguage)) {
        $par_translation = $oneparvalue->getTranslation($this->targetLanguage);
      }
      else {
        $par_translation = NULL;
      }
      $par_fields = $oneparvalue->getFieldDefinitions();
      $par_field_form = [];
      foreach ($par_fields as $par_field_name => $par_field_definition) {
        if (substr($par_field_name, 0, 6) === 'field_') {
          /** @var \Drupal\Core\Entity\EntityDisplayBase $entity_form_display */
          $entity_form_display = $this->entityTypeManager
            ->getStorage('entity_form_display')
            ->load($original->get($field_name)->getFieldDefinition()->getSetting('target_type') . '.' . $oneparvalue->bundle() . '.default');
          $par_field_display = $entity_form_display->getComponent($par_field_name);
          $par_field_form[$par_field_display['weight']] = $par_field_definition;
        }
      }
      // Sort by weight.
      ksort($par_field_form);
      foreach ($par_field_form as $par_field_definition) {
        $par_fieldlist = array_merge($par_fieldlist, $this->retrieveField($par_field_definition->getType(), $par_original, $par_translation, $par_field_definition->getName(), $oneparvalue, $form_state));
      }
    }
    return $par_fieldlist;
  }

  /**
   * Build menu form.
   */
  private function buildFormMenu(array $form, FormStateInterface $form_state) {
    $mids = $this->entityTypeManager->getStorage('menu')->getQuery()
      ->accessCheck(FALSE)
      ->condition('langcode', 'zzz', '<>')
      ->condition('langcode', 'zxx', '<>')
      ->condition('id', 'account', '<>')
      ->condition('id', 'admin', '<>')
      ->condition('id', 'devel', '<>')
      ->condition('id', 'tools', '<>')
      ->sort('label', 'ASC', 'en')
      ->execute();
    $menus = $this->entityTypeManager->getStorage('menu')->loadMultiple($mids);
    /** @var \Drupal\system\Entity\Menu $onemenu */
    foreach ($menus as $onemenu) {

      $fieldlist = [];

      $parameters = new MenuTreeParameters();
      $parameters->setMinDepth(0)->setMaxDepth(4)->onlyEnabledLinks();
      $tree = $this->menuLinkTree->load($onemenu->get('id'), $parameters);
      $manipulators = [
        ['callable' => 'menu.default_tree_manipulators:generateIndexAndSort'],
        ['callable' => 'menu.default_tree_manipulators:flatten'],
      ];
      $tree = $this->menuLinkTree->transform($tree, $manipulators);
      /** @var \Drupal\Core\Menu\MenuLinkTreeElement $element */
      foreach ($tree as $element) {
        $linkDef = $element->link->getPluginDefinition();

        /* empty entity_id */
        if (empty($linkDef['metadata']['entity_id'])) {
          continue;
        }

        $entity_id = $linkDef['metadata']['entity_id'];

        $storage = $this->entityTypeManager
          ->getStorage('menu_link_content');

        /** @var \Drupal\menu_link_content\Entity\MenuLinkContent $entity */
        $entity = $storage->load($entity_id);

        if ($entity->isTranslatable() && $entity->hasTranslation($this->sourceLanguage)) {
          /** @var \Drupal\menu_link_content\Entity\MenuLinkContent $original */
          $original = $entity->getTranslation($this->sourceLanguage);
        }
        else {
          $original = $entity;
        }

        if ($entity->isTranslatable() && $entity->hasTranslation($this->targetLanguage)) {
          /** @var \Drupal\menu_link_content\Entity\MenuLinkContent $translation */
          $translation = $entity->getTranslation($this->targetLanguage);
        }
        else {
          $translation = NULL;
        }
        if ($translation !== NULL) {
          $menu_val = $translation->getTitle() . '<br>' . $translation->getDescription();
        }
        elseif ($form_state->getValue('filluntranslated') === 0) {
          $menu_val = '';
        }
        else {
          $menu_val = $original->getTitle() . '<br>' . $original->getDescription();
        }

        $fieldlist[] = [
          'f' => $entity_id,
          's' => $original->getTitle(),
          't' => $menu_val,
        ];
      }

      $form['tabletmenu'][] = $this->buildFormTable($form, $form_state,
      $onemenu->get('label'), 'machine name: ' . $onemenu->get('id'),
      $fieldlist, 'menu' . $onemenu->get('id'));
    }
    return $form;
  }

  /**
   * Build taxonomy form.
   */
  private function buildFormTaxonomy(array $form, FormStateInterface $form_state) {
    $vocabularies = $this->entityTypeManager->getStorage('taxonomy_vocabulary')->loadMultiple();

    foreach ($vocabularies as $onevocabulary) {
      $tids = $this->entityTypeManager->getStorage('taxonomy_term')->getQuery()
        ->accessCheck(FALSE)
        ->condition('vid', $onevocabulary->get('vid'))
        ->execute();
      $terms = $this->entityTypeManager->getStorage('taxonomy_term')->loadMultiple($tids);
      /** @var \Drupal\taxonomy\Entity\Term $oneterm */
      foreach ($terms as $oneterm) {
        $fieldlist = [];
        if ($oneterm->isTranslatable() && $oneterm->hasTranslation($this->sourceLanguage)) {
          /** @var \Drupal\taxonomy\Entity\Term $original */
          $original = $oneterm->getTranslation($this->sourceLanguage);
        }
        else {
          $original = $oneterm;
        }
        if ($oneterm->isTranslatable() && $oneterm->hasTranslation($this->targetLanguage)) {
          /** @var \Drupal\taxonomy\Entity\Term $translation */
          $translation = $oneterm->getTranslation($this->targetLanguage);
        }
        else {
          $translation = NULL;
        }
        if ($translation !== NULL) {
          $name_val = $translation->getName();
          $desc_val = $translation->getDescription();
        }
        elseif ($form_state->getValue('filluntranslated') === 0) {
          $name_val = '';
          $desc_val = '';
        }
        else {
          $name_val = $original->getName();
          $desc_val = $original->getDescription();
        }

        $fieldlist[] = [
          'f' => 'name',
          's' => $original->getName(),
          't' => $name_val,
        ];
        $fieldlist[] = [
          'f' => 'description',
          's' => $original->getDescription(),
          't' => $desc_val,
        ];

        $form['tablettax'][] = $this->buildFormTable($form, $form_state,
        $onevocabulary->get('name'), 'machine name: ' . $onevocabulary->get('vid') . ', ID: ' . $oneterm->id(),
        $fieldlist, 'taxonomy' . $onevocabulary->get('vid') . $oneterm->id());

      }

    }
    return $form;
  }

  /**
   * Build form table for all types above.
   */
  private function buildFormTable(
    array $form,
    FormStateInterface $form_state,
    $name1,
    $name2,
    array $fieldlist,
    $entityType,
  ) {
    $table = [
      '#type' => 'table',
      '#caption' => [
        '#markup' => '<strong>' . $name1 . ':</strong>  <small>(' . $name2 . ')</small>',
        '#attributes' => ['align' => 'left'],
      ],
      '#header' => [
        ['data' => 'ID', 'width' => '20%'],
        [
          'data' => $this->languageManager->getLanguage($this->sourceLanguage)->getName(),
          'width' => '40%',
        ],
        [
          'data' => $this->languageManager->getLanguage($this->targetLanguage)->getName(),
          'width' => '40%',
        ],
      ],
      '#attributes' => [
        'border' => '1',
        'width' => '100%',
        'class' => ['tsbstable', 'tsbstable_' . $entityType],
      ],
    ];
    foreach ($fieldlist as $k => $onefieldlist) {
      $table = $this->buildFormRow($table, $k, $onefieldlist, $entityType);
    }
    $formtable['table'] = $table;
    $formtable['br'] = [
      '#type' => 'html_tag',
      '#tag' => 'br',
    ];
    return $formtable;
  }

  /**
   * Build form table row.
   */
  private function buildFormRow($table, $k, $onefieldlist, $entityType) {
    if (isset($onefieldlist['a'])) {
      ksort($onefieldlist['a']);
      foreach ($onefieldlist['a'] as $ks => $onefieldlists) {
        $table = $this->buildFormRow($table, $ks, $onefieldlists, $entityType);
      }
      return $table;
    }
    $row = [];
    $row['valign'] = 'top';
    $row['class'][] = 'tsbstable_' . $entityType . '_' . $onefieldlist['f'];

    $r1html = [];
    $r1html['#markup'] = '<small>' . $onefieldlist['f'] . ((isset($onefieldlist['n'])) ? ($onefieldlist['n']) : ('')) . '</small>';
    $row['data'][] = ['data' => $this->renderer->render($r1html)];

    $r2html = [];
    $r2html['#markup'] = $onefieldlist['s'];
    $row['data'][] = [
      'data' => $this->renderer->render($r2html),
      'lang' => $this->sourceLanguage,
    ];

    $r3html = [];
    $r3html['#markup'] = $onefieldlist['t'];
    $row['data'][] = [
      'data' => $this->renderer->render($r3html),
      'lang' => $this->targetLanguage,
    ];

    $table['#rows'][] = $row;
    return $table;
  }

}
