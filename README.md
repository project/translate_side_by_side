CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

This module shows menus, nodes, blocks and taxonomy with source
and target language side by side e.g. to provide a translation template for
an external translation/review/release workflow. All string and text
field types are supported, as well as image (alt, title), file (description)
and link (title). Fields are listed in form display order. Works with fields
in paragraphs.

 * For a full description of the module visit:
   https://www.drupal.org/project/translate_side_by_side

 * To submit bug reports and feature suggestions, or to track changes visit:
   https://www.drupal.org/project/issues/translate_side_by_side


REQUIREMENTS
------------

 * Language module
 * Content_translation module


INSTALLATION
------------

Install the Translate Side by Side module as you would normally install a
contributed Drupal module. Visit https://www.drupal.org/node/1897420 for
further information.

 * Go to Reports > Translate Side by Side
 * Select target language to view all entities


CONFIGURATION
-------------

No configuration required.


MAINTAINERS
-----------

 * sleitner - https://www.drupal.org/u/sleitner
