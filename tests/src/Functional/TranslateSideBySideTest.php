<?php

namespace Drupal\Tests\translate_side_by_side\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests that the translation table works.
 *
 * @group translate_side_by_side
 */
class TranslateSideBySideTest extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'content_translation',
    'block_content',
    'language',
    'menu_ui',
    'menu_link_content',
    'node',
    'taxonomy',
    'translate_side_by_side',
  ];

  /**
   * Default theme.
   *
   * @var string
   */
  protected $defaultTheme = 'stark';

  /**
   * Use the standard profile.
   *
   * @var string
   */
  protected $profile = 'minimal';

  /**
   * Admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected $admin;

  /**
   * Node one.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node1;

  /**
   * Node two.
   *
   * @var \Drupal\node\NodeInterface
   */
  protected $node2;

  /**
   * Block ID.
   *
   * @var int
   */
  protected $blockId1;

  /**
   * Menu ID.
   *
   * @var int
   */
  protected $menuId1;

  /**
   * Term ID.
   *
   * @var string
   */
  protected $termId1;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->admin = $this->drupalCreateUser([], NULL, TRUE);
    $this->drupalLogin($this->admin);

    // Create FR.
    $this->drupalGet('/admin/config/regional/language/add');
    $this->submitForm([
      'predefined_langcode' => 'fr',
    ], 'Add language');

    // Set prefixes to en and fr.
    $this->drupalGet('/admin/config/regional/language/detection/url');
    $this->submitForm([
      'prefix[en]' => 'en',
      'prefix[fr]' => 'fr',
    ], 'Save configuration');

    $this->drupalGet('/admin/config/regional/language/detection');
    $this->submitForm([
      'language_interface[enabled][language-url]' => FALSE,
      'language_content[configurable]' => TRUE,
      'language_content[enabled][language-url]' => TRUE,
    ], 'Save settings');

    // Create content type.
    $this->createContentType([
      'name' => 'Page',
      'type' => 'page',
      'language_configuration[content_translation]' => TRUE,
      'language_configuration[language_alterable]' => TRUE,
    ]);

    // Create custom block type.
    $this->drupalGet('/admin/structure/block-content/add');
    $this->submitForm([
      'label' => 'Basic',
      'id' => 'basic',
      'language_configuration[content_translation]' => TRUE,
      'language_configuration[language_alterable]' => TRUE,
    ], 'Save');

    // Create custom taxonomy.
    $this->drupalGet('/admin/structure/taxonomy/add');
    $this->submitForm([
      'name' => 'Tags',
      'vid' => 'tags',
      'default_language[content_translation]' => TRUE,
      'default_language[language_alterable]' => TRUE,
    ], 'Save');

    // Turn on content translation for taxonomy.
    $this->drupalGet('/admin/config/regional/content-language');
    $this->submitForm([
      'entity_types[node]' => TRUE,
      'settings[node][page][translatable]' => TRUE,
      'settings[node][page][fields][title]' => TRUE,
      'entity_types[block_content]' => TRUE,
      'settings[block_content][basic][translatable]' => TRUE,
      'settings[block_content][basic][fields][body]' => TRUE,
      'entity_types[menu_link_content]' => TRUE,
      'settings[menu_link_content][menu_link_content][translatable]' => TRUE,
      'settings[menu_link_content][menu_link_content][fields][title]' => TRUE,
      'entity_types[taxonomy_term]' => TRUE,
      'settings[taxonomy_term][tags][translatable]' => TRUE,
      'settings[taxonomy_term][tags][fields][name]' => TRUE,
      'settings[taxonomy_term][tags][fields][description]' => TRUE,
    ], 'Save');

    // Create nodes.
    $this->node1 = $this->createNode([
      'title' => 'Node one',
      'type' => 'page',
    ]);
    $this->node2 = $this->createNode([
      'title' => 'Node two',
      'type' => 'page',
    ]);
    // Translate node.
    $this->drupalGet('/fr/node/' . $this->node1->id() . '/translations/add/en/fr');
    $this->submitForm([
      'title[0][value]' => 'Nodule une',
    ], 'Save');

    // Create block.
    $this->drupalGet('/en/block/add');
    $this->submitForm([
      'info[0][value]' => 'Block one info',
      'body[0][value]' => 'Block one body',
    ], 'Save');

    // Translate block.
    $this->drupalGet('/fr/admin/content/block/1/translations/add/en/fr');
    $this->submitForm([
      'body[0][value]' => 'Block une body',
    ], 'Save');

    // Create menu.
    $this->drupalGet('/en/admin/structure/menu/manage/main/add');
    $this->submitForm([
      'title[0][value]' => 'Menu one',
      'link[0][uri]' => '/node/1',
    ], 'Save');
    $this->menuId1 = preg_match("/\d+/", $this->getUrl());
    // Translate menu.
    $this->drupalGet('/fr/admin/structure/menu/item/' . $this->menuId1 . '/edit/translations/add/en/fr');
    $this->submitForm([
      'title[0][value]' => 'Menu une',
    ], 'Save');

    // Create term.
    $this->drupalGet('/en/admin/structure/taxonomy/manage/tags/add');
    $this->submitForm([
      'name[0][value]' => 'Term one',
    ], 'Save');
    $term1link = $this->getSession()->getPage()->find('css', 'DIV[role="contentinfo"] A');
    $term1 = explode('/', $term1link->getAttribute('href'));
    $this->termId1 = $term1[count($term1) - 1];
    // Translate term.
    $this->drupalGet('/fr/taxonomy/term/' . $this->termId1 . '/translations/add/en/fr');
    $this->submitForm([
      'name[0][value]' => 'Term une',
    ], 'Save');
  }

  /**
   * Test translation output.
   */
  public function testTranslation() {
    $this->drupalGet('/en/admin/reports/translate_side_by_side');
    $this->submitForm([
      'target' => 'fr',
    ], 'Load');
    $this->assertSession()->elementContains('css', '.tsbstable_menumain_' . $this->menuId1 . ' td[lang="en"]', 'Menu one');
    $this->assertSession()->elementContains('css', '.tsbstable_menumain_' . $this->menuId1 . ' td[lang="fr"]', 'Menu une');

    $this->assertSession()->elementContains('css', '.tsbstable_node' . $this->node1->id() . '_title td[lang="en"]', 'Node one');
    $this->assertSession()->elementContains('css', '.tsbstable_node' . $this->node1->id() . '_title td[lang="fr"]', 'Nodule une');
    $this->assertSession()->elementContains('css', '.tsbstable_node' . $this->node2->id() . '_title td[lang="en"]', 'Node two');
    $this->assertTrue(($this->getSession()->getPage()->find('css', '.tsbstable_node' . $this->node2->id() . '_title td[lang="fr"]')->getHtml() === ''),
      '.tsbstable_node' . $this->node2->id() . '_title td[lang="fr"] not empty');

    $this->assertSession()->elementContains('css', '.tsbstable_block_content1_body td[lang="en"]', 'Block one body');
    $this->assertSession()->elementContains('css', '.tsbstable_block_content1_body td[lang="fr"]', 'Block une body');

    $this->assertSession()->elementContains('css', '.tsbstable_taxonomytags' . $this->termId1 . '_name td[lang="en"]', 'Term one');
    $this->assertSession()->elementContains('css', '.tsbstable_taxonomytags' . $this->termId1 . '_name td[lang="fr"]', 'Term une');

    // Test: Fill untranslated with source.
    $this->drupalGet('/en/admin/reports/translate_side_by_side');
    $this->submitForm([
      'target' => 'fr',
      'filluntranslated' => TRUE,
    ], 'Load');
    $this->assertSession()->elementContains('css', '.tsbstable_node' . $this->node2->id() . '_title td[lang="fr"]', 'Node two');
  }

}
